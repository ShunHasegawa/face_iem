\Sexpr{set_parent('FACE_IEM_report.Rnw')}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% source files & libraries & setup options                                  %%
%% need to be loaded here if one wants to compile this child document to PDF %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% <<setup, include=FALSE>>=
% opts_chunk$set(concordance = TRUE, warning = FALSE, tidy = TRUE, tidy.opts = list(width.cutoff = 60))
% opts_knit$set(root.dir=normalizePath('../'))
% @
% 
% <<readFiles, include=FALSE>>=
% .libPaths("packrat/lib/x86_64-w64-mingw32//3.1.0") 
% #path to packrat library. knitr use local drive library as
% #default
% source("R/pckg.R")
% source("R//functions.R")
% load("output/data/FACE_IEM.RData")
% load("output//data/postDF.RData")
% source("R/SummaryExlTable.R")
% source("R/Figs.R")
% @

\subsection{Nitrate}

%%% CO2 trt %%%
\subsubsection{CO$_2$ trt}

\begin{figure}[!h]\label{figure:CO2IEM_Nitrate}

\begin{center}

<<Figco2Nitrate, echo=FALSE, fig.height=3, fig.width=6>>=
TrtFg[[1]]
@

\caption{IEM-adsorbed nitrate at CO$_2$ treatments}
\end{center}
\end{figure}

<<Tableco2Nitrate, echo=FALSE, results='asis'>>=
printTbl(TrtSmmryTbl[[1]], 
         caption = "CO2 trt mean of IEM-adsorbed nitrate",
         label = "table:FACE_IEM_CO2_nitrate")
@

%%%%%%%%%%%%%
%%% Stats %%%
%%%%%%%%%%%%%
\clearpage
\paragraph{Stats}

<<ReadScript_FACE_IEM_Nitrate, echo=FALSE, cache=FALSE, include=FALSE>>=
read_chunk('R/Stats_NO.R')
source("R/Stats_NO.R")
@


%%%%%%%%%%%%%
%% Summary %%
%%%%%%%%%%%%%

\paragraph{Pre-CO$_2$}
\noindent

<<Stat_FACE_IEM_Nitrate_preCO2_Smmry, echo=TRUE, results='markup'>>=
@

\paragraph{Post-CO$_2$}
\noindent

<<Stat_FACE_IEM_Nitrate_postCO2_Smmry, echo=TRUE, results='markup'>>=
@

\paragraph{Post-CO$_2$ -ANCOVA with soil variables}
\noindent

<<Stat_FACE_IEM_Nitrate_postCO2_withSoilVar_Smmry, echo=TRUE, results='markup'>>=
@


% %%%%%%%%%%%%
% %% Detail %%
% %%%%%%%%%%%%
% 
% \paragraph{Pre-CO$_2$}
% \noindent
% 
% <<Stat_FACE_IEM_Nitrate_preCO2, echo=TRUE, results='markup'>>=
% @
% 
% \paragraph{Stats}
% \paragraph{Post-CO$_2$}
% \noindent
% 
% <<Stat_FACE_IEM_Nitrate_postCO2, echo=TRUE, results='markup'>>=
% @
% \paragraph{Post-CO$_2$ -ANCOVA with soil variables}
% \noindent
% 
% <<Stat_FACE_IEM_Nitrate_postCO2_withSoilVar, echo=TRUE, results='markup'>>=
% @

%%% Ring %%%
\clearpage
\subsubsection{Ring}

\begin{figure}[!h]\label{figure:RingIEM_nitrate}
\begin{center}

<<FigChNitrate, echo=FALSE, fig.height=3, fig.width=6>>=
RngFg[[1]]
@

\caption{IEM-adsorbed nitrate in each ring}
\end{center}
\end{figure}

<<TableRingNitrate, echo=FALSE, results='asis'>>=
printRngTbl(RngSmmryTbl[[1]], 
            caption = "Ring mean of IEM-adsorbed nitrate", 
            label = "table:FACE_IEM_ring_nitrate",
            size = "small")
@
